/**
 * 
 * 
 * @param {any} a length of side
 * @param {any} b length of side
 * @param {any} c length of side
 * @returns 
 */
function triangleArea(a,b,c){
    // this is Heron's formula
    const p = (a+b+c)/2;
    return Math.sqrt(p*(p-a)*(p-b)*(p-c));
}
console.log(triangleArea(5,6,7));
